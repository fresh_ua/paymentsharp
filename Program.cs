﻿using System;

namespace Payment
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Payment vasya = new Payment ("Василий",
				                "Пупкин",
				                "Александрович",
				                2003,
				                5000, 15);
			Console.WriteLine("Полное имя - "+vasya.name);
			Console.WriteLine ("Стаж - " + vasya.getExpirience ()+" лет");
			Console.WriteLine ("--------------------------------------");
			Console.WriteLine ("Зарплата в день - " + vasya.getCostInDay());
			Console.WriteLine("Начислено зарплаты - " + vasya.getStaged());
			Console.WriteLine("Премии - " + vasya.getExtra());
			Console.WriteLine ("--------------------------------------");
			Console.WriteLine("Подоходный налог - " + vasya.getTax());
			Console.WriteLine("Военный налог - " + vasya.getWarTax());
			Console.WriteLine("Социальный взнос - " + vasya.getSocTax());
			Console.WriteLine ("Общая удержанная сумма - " + vasya.getWithhold ());
			Console.WriteLine ("--------------------------------------");
			Console.WriteLine ("К получению - "+vasya.getTotal());
			Console.WriteLine ("==============================================");
			Console.WriteLine ("==== ==== ==== ==== ==== ==== ==== ==== ==== =");
			Console.WriteLine ("==============================================");
			Payment havansky = new Payment ("Хавански",
				"Хулиганская",
				"Никитична",
				2013,
				7400, 5);
			Console.WriteLine("Полное имя - "+havansky.name);
			Console.WriteLine ("Стаж - " + havansky.getExpirience ()+" лет");
			Console.WriteLine ("--------------------------------------");
			Console.WriteLine ("Зарплата в день - " + havansky.getCostInDay());
			Console.WriteLine("Начислено зарплаты - " + havansky.getStaged());
			Console.WriteLine("Премии - " + havansky.getExtra());
			Console.WriteLine ("--------------------------------------");
			Console.WriteLine("Подоходный налог - " + havansky.getTax());
			Console.WriteLine("Военный налог - " + havansky.getWarTax());
			Console.WriteLine("Социальный взнос - " + havansky.getSocTax());
			Console.WriteLine ("Общая удержанная сумма - " + havansky.getWithhold ());
			Console.WriteLine ("--------------------------------------");
			Console.WriteLine ("К получению - "+havansky.getTotal());
			Console.ReadKey ();
		}
	}
}

