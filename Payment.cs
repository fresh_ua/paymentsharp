﻿using System;

namespace Payment
{
	public class Payment
	{
		/// <summary>
		/// Имя
		/// </summary>
		protected string firstname;

		/// <summary>
		/// Фамилия
		/// </summary>
		protected string lastname;

		/// <summary>
		/// Отчество
		/// </summary>
		protected string middlename;

		/// <summary>
		/// Полное имя
		/// </summary>
		public string name
		{
			get { return lastname + " " + firstname + " " + middlename; }
		}

		/// <summary>
		/// Получает год с которого работает данный сотрудник
		/// </summary>
		/// <value>Год</value>
		protected int year;

		/// <summary>
		/// Получение суммы оклада
		/// </summary>
		/// <value>Оклад</value>
		protected int salary;

		/// <summary>
		/// Процент надбавки
		/// </summary>
		protected int extra;

		/// <summary>
		/// Подоходный налог
		/// </summary>
		protected double tax = 15;

		/// <summary>
		/// Количество отработанных дней
		/// </summary>
		protected int worked;

		/// <summary>
		/// Количество рабочих дней в месяце
		/// </summary>
		protected int workingDays;

		/// <summary>
		/// Начисленная сумма
		/// </summary>
		protected double staged;

		/// <summary>
		/// Удержанная сумма
		/// </summary>
		protected double withhold;

		/// <summary>
		/// Создает новый экземпляр класса <see cref="Payment.Payment"/>
		/// </summary>
		/// <param name="firstname">Имя</param>
		/// <param name="lastname">Фамилия</param>
		/// <param name="middlename">Отчество</param>
		/// <param name="year">Год устройства</param>
		/// <param name="salary">Оклад</param>
		/// <param name="extra">Надбавка (процент)</param>
		public Payment (string firstname, string lastname, string middlename, int year, int salary, int extra)
		{
			this.firstname = firstname;
			this.lastname = lastname;
			this.middlename = middlename;

			this.year = year;
			this.salary = salary;
			this.extra = extra;
			this.calcCountWorkingDays();
		}

		/// <summary>
		/// Количество рабочих дней в месяце
		/// </summary>
		private void calcCountWorkingDays()
		{
			int current_month = DateTime.Now.Month;
			int current_year = DateTime.Now.Year;

			DateTime start = new DateTime(current_year, current_month, 01);
			DateTime end = new DateTime(current_year, current_month, DateTime.DaysInMonth(current_year, current_month));
			for (DateTime i = start; i <= end; i = i.AddDays (1)) {
				if (!(i.DayOfWeek.ToString () == "Saturday" || i.DayOfWeek.ToString () == "Sunday")) {
					this.workingDays++;
				}
			}
		}

		/// <summary>
		/// Сумма премии без вычета налогов
		/// </summary>
		public double getExtra()
		{
			return this.extra * this.salary / 100;
		}

		/// <summary>
		/// Начислено зарплаты
		/// </summary>
		public double getStaged()
		{
			return this.staged = (this.getCostInDay () * this.workingDays) + this.getExtra();
		}

		/// <summary>
		/// Удержанная сумма
		/// </summary>
		public double getWithhold()
		{
			return this.withhold = this.getTotal () - this.staged;
		}

		/// <summary>
		/// Сумма "на руки"
		/// </summary>
		public double getTotal()
		{
			return this.staged - (this.getWarTax () + this.getSocTax () + this.getTax ());
		}

		/// <summary>
		/// Расчет военного налога
		/// </summary>
		public double getWarTax()
		{
			return this.staged * 1.5 / 100;
		}

		/// <summary>
		/// Расчет социального взноса
		/// </summary>
		public double getSocTax()
		{
			return this.staged * 3.6 / 100;
		}

		/// <summary>
		/// Расчет подоходного налога
		/// </summary>
		public double getTax()
		{
			return this.staged * this.tax / 100;
		}

		/// <summary>
		/// Стоимость сотрудника за день
		/// </summary>
		public double getCostInDay()
		{
			return this.salary / this.workingDays;
		}

		/// <summary>
		/// Стаж сотрудника
		/// </summary>
		/// <returns>Стаж сотрудника</returns>
		public int getExpirience()
		{
			return Convert.ToInt32(DateTime.Now.Year-this.year);
		}

	}
}

